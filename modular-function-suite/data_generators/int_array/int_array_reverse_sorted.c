int int_array_reverse_sorted(int data[], int size)
{
    for(int i = 0; i < size; i++)
    {
        data[i] = size-i-1;
    }
    return size;
}