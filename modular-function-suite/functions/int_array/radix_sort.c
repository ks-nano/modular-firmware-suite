int radix_sort_find_max(int array[], int size)
{
    int max = array[0];
    for (int i = 1; i < size; i++)
    {
        if (array[i] > max)
        {
            max = array[i];
        }
    }
    return max;
}

void radix_sort_counting_sort(int array[], int size, int exp)
{
    int output[size];
    int count[10] = {0};

    // Calculate the count of each digit
    for (int i = 0; i < size; i++)
    {
        count[(array[i] / exp) % 10]++;
    }

    // Calculate the cumulative count
    for (int i = 1; i < 10; i++)
    {
        count[i] += count[i - 1];
    }

    // Build the output array
    for (int i = size - 1; i >= 0; i--)
    {
        output[count[(array[i] / exp) % 10] - 1] = array[i];
        count[(array[i] / exp) % 10]--;
    }

    // Copy the sorted elements back to the original array
    for (int i = 0; i < size; i++)
    {
        array[i] = output[i];
    }
}

// Radix Sort function
void radix_sort(int data[], int size)
{
    // Find the maximum element to determine the number of digits
    int max = radix_sort_find_max(data, size);

    // Perform counting sort for each digit from least significant to most significant
    for (int exp = 1; max / exp > 0; exp *= 10)
    {
        radix_sort_counting_sort(data, size, exp);
    }
}
