int counting_sort_find_max(int data[], int size)
{
    int max = data[0];
    for (int i = 1; i < size; i++)
    {
        if (data[i] > max)
        {
            max = data[i];
        }
    }
    return max;

}
void counting_sort(int data[], int size)
{
    int max = counting_sort_find_max(data, size);
    int count[max + 1];
    for (int i = 0; i <= max; i++)
    {
        count[i] = 0;
    }
    for (int i = 0; i < size; i++)
    {
        count[data[i]]++;
    }
    for (int i = 1; i <= max; i++)
    {
        count[i] += count[i - 1];
    }
    int output[size];
    for (int i = size - 1; i >= 0; i--)
    {
        output[count[data[i]] - 1] = data[i];
        count[data[i]]--;
    }
    for (int i = 0; i < size; i++) {
        data[i] = output[i];
    }
}