void bubble_sort(int data[], int size)
{
    int j, temp;
    int notSorted = 1;
    while(notSorted == 1)
    {
        notSorted = 0;
        for (j = 0; j < size - 1; j++) 
        {
            if (data[j] > data[j + 1]) 
            {
                temp = data[j];
                data[j] = data[j + 1];
                data[j + 1] = temp;
                notSorted = 1;
            }
        }
    }
}
