def gen_marker_decls(writer):
    writer.write('volatile int marker_setup_done;\n')
    writer.write('volatile int marker_data_gen_start;\n')
    writer.write('volatile int marker_data_gen_done;\n')
    writer.write('volatile int marker_func_start;\n')
    writer.write('volatile int marker_func_done;\n')

def gen_setup_string_function(writer):
    writer.write("""
#include <ee_printf.h>
void marker_system_print()
{
  ee_printf("setup_done=%p\\n", &marker_setup_done);
  ee_printf("data_gen_start=%p\\n", &marker_data_gen_start);
  ee_printf("data_gen_done=%p\\n", &marker_data_gen_done);
  ee_printf("func_start=%p\\n", &marker_func_start);
  ee_printf("func_done=%p\\n", &marker_func_done);
}
""")

def gen_marker_setup(writer):
    writer.write('asm volatile("": : :"memory");\n')
    writer.write('  marker_setup_done = 0;\n');

def gen_marker_data_gen_start(writer):
    writer.write('asm volatile("": : :"memory");\n')
    writer.write('  marker_data_gen_start=0;\n')
    writer.write('asm volatile("": : :"memory");\n')

def gen_marker_data_gen_done(writer):
    writer.write('asm volatile("": : :"memory");\n')
    writer.write('  marker_data_gen_done=0;\n')

def gen_marker_func_start(writer):
    writer.write('asm volatile("": : :"memory");\n')
    writer.write('  marker_func_start=0;\n')
    writer.write('asm volatile("": : :"memory");\n')

def gen_marker_func_done(writer):
    writer.write('asm volatile("": : :"memory");\n')
    writer.write('  marker_func_done=0;\n')
