def gen_marker_setup(writer):
    writer.write('  volatile int marker;\n')
    writer.write('  for (int i=0;i<10;i++)\n')
    writer.write('  {\n')
    writer.write('    marker=i;\n')
    writer.write('  }\n')
    writer.write('  marker=0;\n')

def gen_marker_function(writer):
    writer.write('  marker=0;\n')