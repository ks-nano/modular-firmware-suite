import argparse
import glob
from os.path import basename
import pkgutil
import os
from importlib import import_module

parser = argparse.ArgumentParser()

root_dir = os.path.dirname(os.path.abspath(__file__))
function_parameters_names = [name for _, name, _ in pkgutil.iter_modules([f'{root_dir}/function_parameters'])]
marker_system_names = [name for _, name, _ in pkgutil.iter_modules([f'{root_dir}/marker_systems'])]

parser.add_argument('--num-items', default=20)
parser.add_argument('--function-parameters', choices=function_parameters_names, required=True)
parser.add_argument('--marker-system', choices=marker_system_names, required=True)
parser.add_argument('--functions-glob', default='*.c')
parser.add_argument('--data-generators-glob', default='*.c')
parser.add_argument('--output-file', required=True)

args = parser.parse_args()



function_parameters = import_module(f'function_parameters.{args.function_parameters}')
marker_system = import_module(f'marker_systems.{args.marker_system}')

function_glob = f'{root_dir}/functions/{args.function_parameters}/{args.functions_glob}'
data_gen_glob = f'{root_dir}/data_generators/{args.function_parameters}/{args.data_generators_glob}'


with open(args.output_file, 'w') as writer:
    function_parameters.gen_storage(writer, args.num_items)
    # generate any includes
    # generate protos
    for path_to_function in glob.glob(function_glob):
        function_name = basename(path_to_function)[:-2]
        function_parameters.write_external_function_decl(writer, function_name)
    for path_to_data_generator in glob.glob(data_gen_glob):
        data_gen_name = basename(path_to_data_generator)[:-2]
        function_parameters.write_external_data_gen_decl(writer, data_gen_name)
    marker_system.gen_marker_decls(writer)
    marker_system.gen_setup_string_function(writer)
    # find all functions & data generators
    data_generator_pathes = [basename(pdg)[:-2] for pdg in glob.glob(data_gen_glob)]
    function_pathes = [basename(fp)[:-2] for fp in glob.glob(function_glob)]
    # generate experiment info
    writer.write('void experiment_info()\n{\n')
    writer.write('  ee_printf("experiment_info=\'");')
    first = True
    for pdg in data_generator_pathes:
        for fp in function_pathes:
            writer.write(f'  ee_printf("{"," if first else ""}{pdg}");\n')
            writer.write(f'  ee_printf(",{fp}");\n')
            first = False
    writer.write('}\n')
    # generate experiment
    writer.write('void experiment()\n{\n')
    marker_system.gen_marker_setup(writer)
    # generate experiment
    for data_gen_name in data_generator_pathes:
        for function_name in function_pathes:
            marker_system.gen_marker_data_gen_start(writer)
            function_parameters.gen_call_data_gen(writer, data_gen_name, args.num_items)
            marker_system.gen_marker_data_gen_done(writer)
            marker_system.gen_marker_func_start(writer)
            function_parameters.gen_call_func(writer, function_name, args.num_items)
            marker_system.gen_marker_func_done(writer)
    writer.write('}\n')
