def gen_storage(writer, num_items):
    writer.write(f'int storage[{num_items}];\n')

def write_external_function_decl(writer, function_name):
    writer.write(f"extern void {function_name}(int data[], int size);\n")

def write_external_data_gen_decl(writer, gen_name):
    writer.write(f"extern int {gen_name}(int data[], int size);\n")

def gen_call_data_gen(writer, data_gen_name, num_items):
    writer.write(f'  {data_gen_name}(storage, {num_items});\n')

def gen_call_func(writer, function_name, num_items):
    writer.write(f'  {function_name}(storage, {num_items});\n')