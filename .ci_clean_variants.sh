#/bin/bash
set -euxo pipefail
echo "Cleaning storage git"
cd tmu_hil_airisc_mfs_firmware/
rm ../storage-dut/airisc/32mhz/mfs-hil/*