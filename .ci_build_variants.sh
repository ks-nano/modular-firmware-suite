#/bin/bash
set -euxo pipefail
echo "Working on $1"
cd tmu_hil_airisc_mfs_firmware/

make clean
make elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__default.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__default.elf

make clean
make TMU_INT_DEBUG=1 elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__int_debug.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__int_debug.elf

make clean
make TMU_POST_DATA=1 elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__int_post_data.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__int_post_data.elf

make clean
make TMU_REENTRANT_CHECK=1 elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__check_reentrant.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__check_reentrant.elf

make clean
make TMU_INT_POST_FLUSH=1 elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__int_post_flush.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__int_post_flush.elf

make clean
make TMU_MODE_ON="((8<<4)+0b0000)" TMU_MODE_OFF="((8<<4)+0b000)" elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_off.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_off.elf

make clean
make TMU_MODE_ON="((9<<4)+0b011)" TMU_MODE_OFF="((9<<4)+0b010)" elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_9_nocompress.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_9_nocompress.elf

make clean
make TMU_MODE_ON="((8<<4)+0b011)" TMU_MODE_OFF="((8<<4)+0b010)" elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_8_nocompress.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_8_nocompress.elf

make clean
make TMU_MODE_ON="((9<<4)+0b0111)" TMU_MODE_OFF="((9<<4)+0b010)" elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_9.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_9.elf

make clean
make TMU_MODE_ON="((8<<4)+0b0111)" TMU_MODE_OFF="((8<<4)+0b010)" elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_8.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_8.elf

make clean
make TMU_MODE_ON="((7<<4)+0b0111)" TMU_MODE_OFF="((7<<4)+0b010)" elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_7.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_7.elf

make clean
make TMU_MODE_ON="((6<<4)+0b0111)" TMU_MODE_OFF="((6<<4)+0b010)" elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_6.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_6.elf

make clean
make TMU_MODE_ON="((5<<4)+0b0111)" TMU_MODE_OFF="((5<<4)+0b010)" elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_5.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_5.elf

make clean
make TMU_MODE_ON="((4<<4)+0b0111)" TMU_MODE_OFF="((4<<4)+0b010)" elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_4.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_4.elf

make clean
make TMU_MODE_ON="((3<<4)+0b0111)" TMU_MODE_OFF="((3<<4)+0b010)" elf
cp $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_3.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__tmu_3.elf

make clean
make elf
mv $1.c ../storage-dut/airisc/32mhz/mfs-hil/$1__default2.c
mv main.elf ../storage-dut/airisc/32mhz/mfs-hil/$1__default2.elf
