#include <stdint.h>
#include <airisc.h>
#include "utils.h"
#include <ee_printf.h> // use "embedded" ee_printf (much smaller!) instead of stdio's printf

#define TICK_TIME  (CLOCK_HZ) // timer tick every 1s
volatile uint32_t uptime;
volatile uint32_t in_int;
extern void experiment();
extern void experiment_info();
extern void marker_system_print();
/*
 * Mostly copied from examples
 */
int main(void)
{
  int i, j;
  in_int = 0;

  // configure all GPIO pins (1 = pin is output; 0 = pin is input)
  gpio0->EN   = -1; // all configured as outputs
  gpio0->DATA =  0; // all LEDs off (assuming high-active LEDs)

  // setup UART0: ee_printf, STDOUT, STDERR and STDIN are mapped to UART0
  uart_init(uart0, UART_DATA_BITS_8, UART_PARITY_EVEN, UART_STOP_BITS_1, UART_FLOW_CTRL_NONE, (uint32_t)(CLOCK_HZ/UART0_BAUD));
  char buf[3];
  while(buf[0]!='g' || buf[1]!='o')
  {
    uart_readData(uart0, (uint8_t*)buf, 2);
    ee_printf("\r\n%s\r\n", buf);
  }
  /* Write infos from SOC */
  ee_printf("\r\nThis is the Fraunhofer IMS AIRISC! :)\r\n");
  ee_printf("Build: "__DATE__" "__TIME__"\r\n\r\n");


  // show RISC-V ISA configuration
  char misa[64];
  get_misa_string(&misa[0]);
  ee_printf("ISA:      %s\r\n", misa);


  // show core IDs
  ee_printf("VENDORID: 0x%08x\r\n", cpu_csr_read(CSR_MVENDORID)); // vendor ID
  ee_printf("ARCHID:   0x%0   8x\r\n", cpu_csr_read(CSR_MARCHID)); // architecture ID
  ee_printf("IMPID:    0x%08x\r\n", cpu_csr_read(CSR_MIMPID)); // implemantation ID
  ee_printf("HARTID:   0x%08x\r\n", cpu_csr_read(CSR_MHARTID)); // hart ID
  ee_printf("\r\n\r\n");


  // setup CPU counters
  ee_printf("Initializing CPU counters...\r\n");
  cpu_csr_write(CSR_MCOUNTINHIBIT, -1); // stop all counters
  cpu_csr_write(CSR_MCYCLE, 0);
  cpu_csr_write(CSR_MCYCLEH, 0);
  cpu_csr_write(CSR_MINSTRET, 0);
  cpu_csr_write(CSR_MINSTRETH, 0);
  cpu_csr_write(CSR_MCOUNTINHIBIT, 0); // enable all counters


  // start true random number generator and print some data
  ee_printf("Starting TRNG... ");
  trng_enable(trng);
  if (trng_is_enabled(trng)) { // check if TRNG is really enabled
    ee_printf("ok\r\n");
    if (trng_is_sim(trng)) { // check if TRNG is in simulation mode
      ee_printf("[WARNING] TRNG is in SIMULATION mode!\r\n");
    }

    ee_printf("TRNG data demo:\r\n");
    for (i=0; i<8; i++) {
      for (j=0; j<16; j++) {
        ee_printf("0x%02x ", trng_get(trng));
      }
      ee_printf("\r\n");
    }
    trng_disable(trng); // shutdown TRNG
  }
  else {
    ee_printf("FAILED\r\n");
  }


  // configure and enable MTIME timer interrupt
  ee_printf("Configuring system tick timer...\r\n");
  uptime = 0;
  timer_set_time(timer0, 0); // reset time counter
  timer_set_timecmp(timer0, (uint64_t)(TICK_TIME)); // set timeout for first tick


  // print number of implemented/available externl interrupt channels
  ee_printf("Checking available external interrupt lines... %u\r\n", get_num_xirq());


  // enable CPU interrupts
  ee_printf("Configuring interrupts...\r\n");
  uint32_t tmp = 0;
  //tmp |= (1 << IRQ_MTI); // machine timer interrupt
  tmp |= (1 << IRQ_XIRQ0) | (1 << IRQ_XIRQ1); // AIRISC-specific external interrupt channel 0 and 1
  tmp |= (1 << IRQ_XIRQ2) | (1 << IRQ_XIRQ3); // AIRISC-specific external interrupt channel 2 and 3
  tmp |= (1 << IRQ_XIRQ4) | (1 << IRQ_XIRQ5); // AIRISC-specific external interrupt channel 4 and 5
  tmp |= (1 << IRQ_XIRQ6) | (1 << IRQ_XIRQ7); // AIRISC-specific external interrupt channel 6 and 7
  cpu_csr_write(CSR_MIE, tmp); // enable interrupt sources


  // endless counter loop using busy-wait
  ee_printf("Enabling IRQs...\r\n\r\n");
  cpu_csr_set(CSR_MSTATUS, 1 << MSTATUS_MIE); // enable machine-mode interrupts
  
  /* HIL: START TEST */
  ee_printf("[HIL] CONFIG START\r\n");
  ee_printf("TMU_INT_READOUT_START='T%'\r\n");
  ee_printf("TMU_INT_READOUT_END='%T'\r\n");
  ee_printf("TMU_INT_READOUT_END_NEWLINE=TRUE\r\n");
  ee_printf("TMU_MODE_ON=%x\r\n", TMU_MODE_ON);
  ee_printf("TMU_MODE_OFF=%x\r\n", TMU_MODE_OFF);
  ee_printf("TMU_BASE_ADDR=%x\r\n", TMU_BASE_ADDR);
  ee_printf("TMU_INT_DEBUG=%x\r\n", TMU_INT_DEBUG);
  ee_printf("TMU_INT_POST_FLUSH=%x\r\n", TMU_INT_POST_FLUSH);
  ee_printf("TMU_REENTRANT_CHECK=%x\r\n", TMU_REENTRANT_CHECK);
  ee_printf("TMU_POST_DATA=%x\r\n", TMU_POST_DATA);
  //ee_printf("EXPERIMENT_NAME=test.c\r\n");
  marker_system_print();
  experiment_info();
  ee_printf("[HIL] CONFIG END\r\n");
  ee_printf("[HIL] START\r\n");
  tmu_set_mode(TMU_BASE_ADDR, TMU_MODE_ON);
  experiment();
  tmu_set_mode(TMU_BASE_ADDR, TMU_MODE_OFF);
  ee_printf("[HIL] END\r\n");
  while(1)
  {
      asm volatile("nop");
//      tmu_set_mode(TMU_BASE_ADDR, TMU_MODE_OFF);
//      ee_printf("[HIL] END\r\n");
  }
  return 0;
}

void __attribute__ ((__interrupt__, aligned(4))) __trap_entry(void)
{
        uint32_t mcause = cpu_csr_read(CSR_MCAUSE);
        uint32_t mepc   = cpu_csr_read(CSR_MEPC);
        uint32_t mtval  = cpu_csr_read(CSR_MTVAL);

        if (mcause & 0x80000000UL) { // is interrupt (async. exception)
                interrupt_handler(mcause, mepc);
        }
        else { // is (sync.) exception
                exception_handler(mcause, mepc, mtval);
        }
}


/**********************************************************************//**
 * Custom interrupt handler (overriding the default DUMMY handler from "airisc.c").
 *
 * @note This is a "normal" function - so NO 'interrupt' attribute!
 *
 * @param[in] cause Exception identifier from mcause CSR.
 * @param[in] epc Exception program counter from mepc CSR.
 **************************************************************************/
inline __attribute__((__always_inline__)) void interrupt_handler(uint32_t cause, uint32_t epc) {
  #if (TMU_REENTRANT_CHECK > 0)
  if (in_int!=0)
  {
      ee_printf("[HIL] DEAD: Already in interrupt Cause %u\r\n",cause);
      while(1);
  }
  in_int = 1;
  #endif
//  uart_writeByte(uart0, 'I');
//  uart_writeByte(uart0, '\n');
  #if (TMU_INT_DEBUG > 0)
  ee_printf("Cause %u\r\n", cause);
  #endif
  switch(cause) {

    // -------------------------------------------------------
    // Machine timer interrupt (RISC-V-specific)
    // -------------------------------------------------------
    case MCAUSE_TIMER_INT_M:

      // adjust timer compare register for next interrupt
      // this also clears/acknowledges the current machine timer interrupt
      timer_set_timecmp(timer0, timer_get_time(timer0) + (uint64_t)TICK_TIME);

      ee_printf("Uptime: %is\r\n", ++uptime);

      break;

    // -------------------------------------------------------
    // External interrupt (AIRISC-specific)
    // -------------------------------------------------------
    case MCAUSE_XIRQ0_INT:
    case MCAUSE_XIRQ1_INT:
        uart_writeByte(uart0, 'T');
        //uint32_t tmp = tmu_get_mode(TMU_BASE_ADDR);
        tmu_set_mode(TMU_BASE_ADDR, TMU_MODE_OFF);
        //uart_writeByte(uart0, '\n');
        uart_writeByte(uart0, '%');
        uint32_t cnt=0;
        while(tmu_get_status(TMU_BASE_ADDR)!=0b010)
        {
            #if (TMU_INT_DEBUG==0)
            uart_writeData(uart0, (const uint8_t*)tmu_get_data0_ptr(TMU_BASE_ADDR), 4);
            uart_writeData(uart0, (const uint8_t*)tmu_get_data1_ptr(TMU_BASE_ADDR), 4);
            #endif
            tmu_set_next(TMU_BASE_ADDR, 1);
            cnt++;
	    if (cnt>100)
	    {
		ee_printf("[HIL] DEAD TMU STATUS not 0b010 after 100 tries (%x)", tmu_get_status(TMU_BASE_ADDR));
		while(1);
	    }
        }
        #if (TMU_POST_DATA > 0)
        uart_writeData(uart0, (const uint8_t*)tmu_get_data0_ptr(TMU_BASE_ADDR), 4);
        uart_writeData(uart0, (const uint8_t*)tmu_get_data1_ptr(TMU_BASE_ADDR), 4);
	#endif
        #if (TMU_INT_DEBUG > 0)
        ee_printf("%d,%d\n", cnt, cpu_csr_read(CSR_MCYCLE));
        #endif
        uart_writeByte(uart0, '%');
        uart_writeByte(uart0, 'T');
        uart_writeByte(uart0, '\n'); //  war letztes mal aus
        cpu_csr_write(CSR_MIP, cpu_csr_read(CSR_MIP) & (~(1 << ((cause & 0xf) + IRQ_XIRQ0))));
        #if (TMU_INT_DEBUG > 0)
        uart_writeByte(uart0, 'O');
        #endif
        #if (TMU_INT_POST_FLUSH > 0)
            tmu_set_next(TMU_BASE_ADDR, 1);
        #endif
        tmu_set_mode(TMU_BASE_ADDR, TMU_MODE_ON);
        #if (TMU_REENTRANT_CHECK > 0)
        in_int=0;
	#endif
        break;
    case MCAUSE_XIRQ2_INT:
    case MCAUSE_XIRQ3_INT:
    case MCAUSE_XIRQ4_INT:
    case MCAUSE_XIRQ5_INT:
    case MCAUSE_XIRQ6_INT:
    case MCAUSE_XIRQ7_INT:
    case MCAUSE_XIRQ8_INT:
    case MCAUSE_XIRQ9_INT:
    case MCAUSE_XIRQ10_INT:
    case MCAUSE_XIRQ11_INT:
    case MCAUSE_XIRQ12_INT:
    case MCAUSE_XIRQ13_INT:
    case MCAUSE_XIRQ14_INT:
    case MCAUSE_XIRQ15_INT:

      // the lowest 4-bit of MCAUSE identify the actual XIRQ channel when MCAUSE == MCAUSE_XIRQ*_INT
      ee_printf("[HIL] DEAD: External interrupt from channel %u\r\n", (cause & 0xf));
      while(1);
      // clear/acknowledge the current interrupt by clearing the according MIP bit
      cpu_csr_write(CSR_MIP, cpu_csr_read(CSR_MIP) & (~(1 << ((cause & 0xf) + IRQ_XIRQ0))));
      in_int=0;
      break;

    // -------------------------------------------------------
    // Invalid (not implemented) interrupt source
    // -------------------------------------------------------
    default:
      // invalid/unhandled interrupt - give debug information and halt
      ee_printf("[HIL] DEAD: Unknown interrupt source! mcause=0x%08x epc=0x%08x\r\n", cause, epc);

      cpu_csr_write(CSR_MIE, 0); // disable all interrupt sources
      while(1); // halt and catch fire
  }
}


/**********************************************************************//**
 * Custom exception handler (overriding the default DUMMY handler from "airisc.c").
 *
 * @note This is a "normal" function - so NO 'interrupt' attribute!
 *
 * @param[in] cause Exception identifier from mcause CSR.
 * @param[in] epc Exception program counter from mepc CSR.
 * @param[in] tval Trap value from mtval CSR.
 **************************************************************************/
void exception_handler(uint32_t cause, uint32_t epc, uint32_t tval) {

  ee_printf("[HIL] DEAD: UNHANDLED EXCEPTION cause=0x%08x, epc=0x%08x, tval=0x%08x !!%x\r\n", cause, epc, tval);

  cpu_csr_write(CSR_MIE, 0); // disable all interrupt sources
  while(1); // halt and catch fire!

}
